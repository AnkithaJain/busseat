﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Busseat.Startup))]
namespace Busseat
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
